<?php

namespace Drupal\commerce_admin_checkout\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->settings = $config_factory->get('commerce_admin_checkout.settings');
  }


  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('commerce_checkout.form')) {
      $route->setRequirement('_custom_access', '\Drupal\commerce_admin_checkout\CheckoutAccessHandler::checkAccess');
    }
    $override_admin_order_create = (bool) $this->settings->get('override_admin_order_create');
    if ($override_admin_order_create && $route = $collection->get('entity.commerce_order.add_page')) {
      // Override the standard Add new order button to go to the admin checkout system.
      $route->setDefault('_controller', '\Drupal\commerce_admin_checkout\Controller\CartController::createNewCartAndRedirectToCheckout');
    }
  }
}
