<?php

namespace Drupal\commerce_admin_checkout\Controller;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_store\Resolver\StoreResolverInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CartController extends ControllerBase {

  /**
   * @var \Drupal\commerce_cart\CartProvider
   */
  protected $cartProvider;

  /**
   * The order storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;

  /**
   * @var \Drupal\commerce_store\Resolver\StoreResolverInterface
   */
  protected $storeResolver;

  /**
   * CartController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_cart\CartProviderInterface $cartProvider
   * @param \Drupal\commerce_store\Resolver\StoreResolverInterface $storeResolver
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CartProviderInterface $cartProvider, StoreResolverInterface $storeResolver) {
    $this->cartProvider = $cartProvider;
    $this->storeResolver = $storeResolver;
    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('commerce_store.default_store_resolver')
    );
  }

  /**
   * Create a new cart and redirect for processing a customer order.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createNewCartAndRedirectToCheckout() {
    // If the user has permission to checkout as a different user, make the initial cart user "anonymous".
    // Otherwise, the cart should be owned current user.
    $cart_account = $this->currentUser()->hasPermission('access checkout as a different user') ? User::getAnonymousUser() : $this->currentUser();
    $cart = $this->createCart('default', $this->storeResolver->resolve(), $cart_account);
    return $this->redirect('commerce_checkout.form', ['commerce_order' => $cart->id()]);
  }

  /**
   * Create a cart, and unlike DefaultCartProvider, allow duplicates.
   *
   * @param $order_type
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   * @param \Drupal\Core\Session\AccountInterface|NULL $account
   *
   * @return \Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createCart($order_type, StoreInterface $store, AccountInterface $account = NULL) {
    $account = $account ?: $this->currentUser;
    $uid = $account->id();
    $store_id = $store->id();

    // Create the new cart order.
    $cart = $this->orderStorage->create([
      'type' => $order_type,
      'store_id' => $store_id,
      'uid' => $uid,
      'cart' => TRUE,
    ]);
    $cart->save();

    return $cart;
  }

}
