<?php
namespace Drupal\commerce_admin_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_admin_checkout\Element\AdminCheckoutOrderItemsForm;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the contact information pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_admin_checkout_order_items",
 *   label = @Translation("Order Items"),
 *   default_step = "order_information",
 *   wrapper_element = "fieldset",
 * )
 */
class AdminOrderItemsPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow
    );
    $instance->setCurrentUser($container->get('current_user'));
    $instance->setBundleInfo($container->get('entity_type.bundle.info'));
    return $instance;
  }

  /**
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   */
  public function setCurrentUser(AccountProxyInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundleInfo
   */
  public function setBundleInfo(\Drupal\Core\Entity\EntityTypeBundleInfo $bundleInfo) {
    $this->bundleInfo = $bundleInfo;
  }


  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $order_item_bundles = array_map(function ($info) {
      return $info['label'];
    }, $this->bundleInfo->getBundleInfo('commerce_order_item'));

    $form['order_item_types'] = [
      '#type' => 'checkboxes',
      '#options' => $order_item_bundles,
      '#title' => $this->t('Allowed order items types'),
      '#description' => $this->t('Select the order item types that the user can create. Leaving this blank will enable all types.'),
      '#default_value' => $this->configuration['order_item_types'] ?? [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['order_item_types'] = array_filter($values['order_item_types']);
    }
  }

  public function buildConfigurationSummary() {
    if (empty($this->configuration['order_item_types'])) {
      $summary = $this->t('All order items types can be created.');
    }
    else {
      $order_item_bundles = array_map(function ($info) {
        return $info['label'];
      }, $this->bundleInfo->getBundleInfo('commerce_order_item'));
      $labels = [];
      foreach ($this->configuration['order_item_types'] as $type) {
        $labels[] = $order_item_bundles[$type];
      }
      $summary = $this->t('Order item types allowed: @types', [
        '@types' => implode(', ', $labels),
      ]);
    }
    return $summary;
  }


  /**
   * @inheritDoc
   */
  public function isVisible() {
    return $this->currentUser->hasPermission('edit cart items during checkout');
  }


  /**
   * @inheritDoc
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['form'] = [
      '#type' => 'commerce_admin_checkout_order_items_form',
      '#order_id' => $this->order->id(),
      '#order_item_types' => $this->configuration['order_item_types'] ?? [],
      '#element_ajax' => [
        [CheckoutFlowWithPanesBase::class, 'ajaxRefreshPanes'],
      ],
    ];
    return $pane_form;
  }

  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $items = $form_state->getValue(['commerce_admin_checkout_order_items', 'form', 'items']);
    foreach ($items as $order_item_id => $values) {
      if ($order_item_id === 'new') {
        continue;
      }
      AdminCheckoutOrderItemsForm::updateOrderItem($order_item_id, $values);
    }
    parent::submitPaneForm($pane_form, $form_state, $complete_form);
  }


}
