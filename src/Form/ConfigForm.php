<?php

namespace Drupal\commerce_admin_checkout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * ConfigForm constructor.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   */
  public function __construct(RouteBuilderInterface $routerBuilder) {
    $this->routerBuilder = $routerBuilder;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder')
    );
  }


  protected function getEditableConfigNames() {
    return ['commerce_admin_checkout.settings'];
  }

  public function getFormId() {
    return 'commerce_admin_checkout_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('commerce_admin_checkout.settings');

    $form['override_admin_order_create'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace the admin order create form with the customer-facing checkout process'),
      '#default_value' => (bool) $config->get('override_admin_order_create'),
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('commerce_admin_checkout.settings');

    $config->set('override_admin_order_create', (bool) $form_state->getValue('override_admin_order_create'))
      ->save();
    $this->routerBuilder->rebuild();
  }


}
